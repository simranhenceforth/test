package com.example.testinterviewapplication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.testinterviewapplication.R
import com.example.testinterviewapplication.Utils.ApplicationGlobal
import com.example.testinterviewapplication.Utils.CommonMethods
import com.example.testinterviewapplication.activity.HomeActivity
import com.example.testinterviewapplication.webservices.DataClass
import com.example.testinterviewapplication.webservices.RestClient
import kotlinx.android.synthetic.main.fragment_register.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RegisterFragment : BasePhotoUploadFragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun getImage(uri: String?) {
        profile_image = File(uri)
        Glide.with(activity!!).load(profile_image).into(ivImage)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnRegister.setOnClickListener(this)
        tvLogin.setOnClickListener(this)
        ivImage.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivImage -> showImageDialog()
            R.id.btnRegister -> {
                if (etName.text.toString().trim() == "")
                    Toast.makeText(activity!!, "Please enter your name", Toast.LENGTH_SHORT).show()
                else if (etArea.text.toString().trim() == "")
                    Toast.makeText(activity!!, "Please enter area", Toast.LENGTH_SHORT).show()
                else if (etPhoneNumber.text.toString().trim() == "")
                    Toast.makeText(activity!!, "Please enter phone number", Toast.LENGTH_SHORT)
                        .show()
                else if (etPassword.text.toString().trim() == "")
                    Toast.makeText(activity!!, "Please enter password", Toast.LENGTH_SHORT).show()
                else if (etConfirmPassword.text.toString().trim() == "")
                    Toast.makeText(activity!!, "Please enter confirm password", Toast.LENGTH_SHORT)
                        .show()
                else if (profile_image == null)
                    Toast.makeText(activity!!, "Please select image", Toast.LENGTH_SHORT).show()
                else register()
            }
            else -> {
                fragmentManager!!.beginTransaction().replace(R.id.framlayout, LoginFragment())
                    .addToBackStack("").commit()
            }
        }
    }

    private fun register() {
        CommonMethods.showProgressDialog(activity!!)
        val picRequestBody =
            RequestBody.create(MediaType.parse("multipart/form-data"), profile_image!!)
        var pic =
            MultipartBody.Part.createFormData("image", profile_image!!.name, picRequestBody)
        var name = RequestBody.create(MediaType.parse("text/plain"), etName.text.toString())
        var area = RequestBody.create(MediaType.parse("text/plain"), etArea.text.toString())
        var phoneNumber =
            RequestBody.create(MediaType.parse("text/plain"), etPhoneNumber.text.toString())
        var password = RequestBody.create(MediaType.parse("text/plain"), etPassword.text.toString())
        var state = RequestBody.create(MediaType.parse("text/plain"), etState.text.toString())
        var city = RequestBody.create(MediaType.parse("text/plain"), etCity.text.toString())
        
        RestClient.get().register(name, area, phoneNumber, password, pic, state, city)
            .enqueue(object : Callback<DataClass.RegisterResponse> {
                override fun onFailure(call: Call<DataClass.RegisterResponse>, t: Throwable) {
                    CommonMethods.dismissProgressDialog()
                    Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<DataClass.RegisterResponse>,
                    response: Response<DataClass.RegisterResponse>) {
                    CommonMethods.dismissProgressDialog()
                    try {
                        if (response.isSuccessful && response.body() != null) {
                            if(response.body()!!.success) {
                                ApplicationGlobal.profile = response.body()!!.data.items[0]
                                startActivity(Intent(activity, HomeActivity::class.java))
                                activity!!.finish()
                            }else Toast.makeText(activity!!, response.body()!!.message,Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: Exception) {
                        e.message
                    }
                }

            })

    }

}
