package com.example.testinterviewapplication.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.testinterviewapplication.R
import com.example.testinterviewapplication.Utils.ApplicationGlobal
import com.example.testinterviewapplication.Utils.CommonMethods
import com.example.testinterviewapplication.activity.HomeActivity
import com.example.testinterviewapplication.webservices.DataClass
import com.example.testinterviewapplication.webservices.RestClient
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.etPassword
import kotlinx.android.synthetic.main.fragment_login.etPhoneNumber
import kotlinx.android.synthetic.main.fragment_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginFragment : Fragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnLogin.setOnClickListener(this)
        tvRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btnLogin ->{
                if(etPhoneNumber.text.toString() == "")
                    Toast.makeText(activity!!, "Please enter phone number", Toast.LENGTH_SHORT)
                        .show()
                else if (etPassword.text.toString().trim() == "")
                    Toast.makeText(activity!!, "Please enter password", Toast.LENGTH_SHORT).show()
                else login()
            }else ->{
            fragmentManager!!.beginTransaction().replace(R.id.framlayout, RegisterFragment())
                .addToBackStack("").commit()
        }
        }
    }

    private fun login(){
        CommonMethods.showProgressDialog(activity!!)
        var jsonObject= JsonObject()
        jsonObject.addProperty("phone", etPhoneNumber.text.toString())
        jsonObject.addProperty("password", etPassword.text.toString())
        RestClient.get().login(jsonObject )
            .enqueue(object : Callback<DataClass.Login>{
                override fun onFailure(call: Call<DataClass.Login>, t: Throwable) {
                    CommonMethods.dismissProgressDialog()
                }

                override fun onResponse(call: Call<DataClass.Login>, response: Response<DataClass.Login>) {
                    CommonMethods.dismissProgressDialog()
                    try {
                        if (response.isSuccessful && response.body() != null) {
                            startActivity(Intent(activity, HomeActivity::class.java))
                            activity!!.finish()
                        }
                    } catch (e: Exception) {
                        e.message
                    }
                }

            })


    }

}
