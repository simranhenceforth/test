package com.example.testinterviewapplication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide

import com.example.testinterviewapplication.R
import com.example.testinterviewapplication.Utils.ApplicationGlobal
import com.example.testinterviewapplication.Utils.CommonMethods
import com.example.testinterviewapplication.webservices.DataClass
import com.example.testinterviewapplication.webservices.RestClient
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.etArea
import kotlinx.android.synthetic.main.fragment_profile.etCity
import kotlinx.android.synthetic.main.fragment_profile.etName
import kotlinx.android.synthetic.main.fragment_profile.etPhoneNumber
import kotlinx.android.synthetic.main.fragment_profile.etState
import kotlinx.android.synthetic.main.fragment_profile.ivImage
import kotlinx.android.synthetic.main.fragment_register.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.lang.Exception

class ProfileFragment : BasePhotoUploadFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun getImage(uri: String?) {
        profile_image= File(uri)
        Glide.with(activity!!).load(uri).into(ivImage)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(ApplicationGlobal.profile!=null){
            Glide.with(activity!!).load(ApplicationGlobal.profile!!.image).into(ivImage)

            etName.setText(ApplicationGlobal.profile!!.name)
            etArea.setText(ApplicationGlobal.profile!!.area)
            etPhoneNumber.setText(ApplicationGlobal.profile!!.phone)
            etState.setText(ApplicationGlobal.profile!!.state)
            etCity.setText(ApplicationGlobal.profile!!.city)
            etAddress.setText(ApplicationGlobal.profile!!.address)
        }

        btnSave.setOnClickListener { update() }
        ivImage.setOnClickListener { showImageDialog() }
    }

    private fun update(){
        CommonMethods.showProgressDialog(activity!!)

        val picRequestBody =
            RequestBody.create(MediaType.parse("multipart/form-data"), profile_image!!)
        var pic =
            MultipartBody.Part.createFormData("image", profile_image!!.name, picRequestBody)
        var name = RequestBody.create(MediaType.parse("text/plain"), etName.text.toString())
        var area = RequestBody.create(MediaType.parse("text/plain"), etArea.text.toString())
        var phoneNumber =
            RequestBody.create(MediaType.parse("text/plain"), etPhoneNumber.text.toString())
        var state = RequestBody.create(MediaType.parse("text/plain"), etState.text.toString())
        var city = RequestBody.create(MediaType.parse("text/plain"), etCity.text.toString())
        var address = RequestBody.create(MediaType.parse("text/plain"), etAddress.text.toString())

        RestClient.get().update(name,area,phoneNumber,pic,state,city,address).enqueue(object :
            Callback<DataClass.RegisterResponse>{
            override fun onFailure(call: Call<DataClass.RegisterResponse>, t: Throwable) {
                CommonMethods.dismissProgressDialog()
                Toast.makeText(activity!!,t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<DataClass.RegisterResponse>, response: Response<DataClass.RegisterResponse>
            ) {
                CommonMethods.dismissProgressDialog()
                try {
                    if(response.isSuccessful && response.body()!=null){
                        if(response.body()!!.success){
                            ApplicationGlobal.profile=response.body()!!.data.items[0]
                            Toast.makeText(activity!!,"Profile saved", Toast.LENGTH_SHORT).show()
                        }
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }
        })
    }

}
