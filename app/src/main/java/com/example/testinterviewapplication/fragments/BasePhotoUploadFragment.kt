package com.example.testinterviewapplication.fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.testinterviewapplication.Utils.CommonMethods
import java.io.File

abstract class BasePhotoUploadFragment : Fragment() {

    var profile_image: File? = null

    fun init() {}

    fun showImageDialog() {

        if (checkForReadPermission()) {
            if (checkForWritePermission()) openGallery()
        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        when (requestCode) {
            1 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkForWritePermission()) {
                    openGallery()
                }
            }
            2 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                if (checkForWritePermission()) {
                openGallery()
            }
        }
    }

    private fun checkForReadPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(
                activity!!, Manifest.permission.READ_EXTERNAL_STORAGE
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                1
            )
            false
        } else {
            true
        }
    }

    private fun checkForWritePermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
            false
        } else {
            true
        }
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (data != null) {
                if (CommonMethods.getRealPath(data.data!!, activity!!) != null) {
                    getImage(CommonMethods.getRealPath(data.data!!, activity!!))
                }
            }
        }
    }

    abstract fun getImage(uri: String?)


    fun openGallery() {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, 1)
    }

}
