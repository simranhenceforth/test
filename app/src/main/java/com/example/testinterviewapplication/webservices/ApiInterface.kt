package com.example.testinterviewapplication.webservices

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.*

interface ApiInterface {

    @Multipart
    @PUT("user")
    fun register( @Part("name" ) name: RequestBody,
                  @Part("area" ) area: RequestBody,
                  @Part("phone" ) phone: RequestBody,
                  @Part("password" ) password: RequestBody,
                  @Part image: MultipartBody.Part,
                  @Part("state" ) state: RequestBody,
                  @Part("city" ) city: RequestBody):Call<DataClass.RegisterResponse>

    @POST("user/login")
    fun login(@Body login: JsonObject):Call<DataClass.Login>

    @Multipart
    @PATCH("user")
    fun update(@Part("name" ) name: RequestBody,
               @Part("area" ) area: RequestBody,
               @Part("phone" ) phone: RequestBody,
               @Part image: MultipartBody.Part,
               @Part("state" ) state: RequestBody,
               @Part("city" ) city: RequestBody,
               @Part("address" ) address: RequestBody):Call<DataClass.RegisterResponse>

}