package com.example.testinterviewapplication.webservices

class DataClass {

    data class RegisterResponse(
        var success: Boolean,
        var message:String,
        var data: Data
    )

    data class Data(
        var count:Int,
        var items:ArrayList<Profile>
    )

    data class Login(
        var success: Boolean,
        var message:String,
        var data: LoginData
    )

    data class LoginData(
        var count:Int,
        var items:HashMap<String,Profile>
    )

    data class Profile(
        var id:String,
        var name:String,
        var phone:String,
        var state:String,
        var city:String,
        var area:String,
        var image:String,
        var address:String,
        var isActive:Boolean,
        var isVerified:Boolean,
        var createdOn:String,
        var modifiedOn:String
    )


}