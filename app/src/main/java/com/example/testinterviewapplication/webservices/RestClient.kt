package com.example.testinterviewapplication.webservices

import android.os.Build
import androidx.annotation.RequiresApi
import com.example.testinterviewapplication.Utils.ApplicationGlobal
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RestClient {
    companion object {
        private lateinit var retrofit: Retrofit
        private lateinit var REST_CLIENT: ApiInterface
        private var API_URL ="http://clientapps.webhopers.com:3333/api/app/"

        @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
        fun get(): ApiInterface {
            retrofit = Retrofit.Builder()
                .baseUrl(API_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            REST_CLIENT = retrofit.create(ApiInterface::class.java)
            return REST_CLIENT
        }

        @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
        fun getOkHttpClient(): OkHttpClient {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val builder = OkHttpClient.Builder()
            builder.connectTimeout(5, TimeUnit.MINUTES)
            builder.readTimeout(5, TimeUnit.MINUTES)
            builder.addNetworkInterceptor(httpLoggingInterceptor)
            builder.addInterceptor { chain ->
                val request = chain.request()
                val header = request.newBuilder()?.header("Authorization",
                    ApplicationGlobal.sessionId)
                val build = header!!.build()
                chain!!.proceed(build)
            }
            return builder.build()
        }

        fun getRetrofitInstance(): Retrofit {
            return retrofit
        }
    }

}