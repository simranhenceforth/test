package com.example.testinterviewapplication.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testinterviewapplication.R
import com.example.testinterviewapplication.fragments.LoginFragment
import com.example.testinterviewapplication.fragments.RegisterFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().add(R.id.framlayout, LoginFragment())
            .addToBackStack("").commit()
    }
}
