package com.example.testinterviewapplication.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.testinterviewapplication.R
import com.example.testinterviewapplication.Utils.ApplicationGlobal
import com.example.testinterviewapplication.fragments.ProfileFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        supportFragmentManager.beginTransaction().add(R.id.framMain, ProfileFragment()).commit()
    }
}
