package com.example.testinterviewapplication.Utils

import android.app.Application
import com.example.testinterviewapplication.webservices.DataClass

class ApplicationGlobal : Application() {

    companion object{
        var sessionId=""
        var profile: DataClass.Profile?=null
    }
}