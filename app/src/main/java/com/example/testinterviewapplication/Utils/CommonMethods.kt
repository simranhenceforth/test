package com.example.testinterviewapplication.Utils

import android.app.Dialog
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.view.Window
import androidx.annotation.RequiresApi
import com.example.testinterviewapplication.R
import java.io.File

class CommonMethods {

    companion object {

        private fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri
                .authority
        }

        private fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri
                .authority
        }

        private fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri
                .authority
        }

        private fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?
        ): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(column)
            try {
                cursor = context.contentResolver.query(
                    uri!!, projection,
                    selection, selectionArgs, null
                )
                if (cursor != null && cursor.moveToFirst()) {
                    val column_index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(column_index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        fun getPathToNonPrimaryVolume(context: Context, tag: String): String? {
            val volumes = context.externalCacheDirs
            if (volumes != null) {
                for (volume in volumes) {
                    if (volume != null) {
                        val path = volume.absolutePath
                        if (path != null) {
                            val index = path.indexOf(tag)
                            if (index != -1) {
                                return path.substring(0, index) + tag
                            }
                        }
                    }
                }
            }
            return null
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        fun getRealPath(uri: Uri, context: Context): String? {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return (Environment.getExternalStorageDirectory()
                            .toString() + "/"
                                + split[1])
                    } else {
                        val splitIndex = docId.indexOf(':', 1)
                        val tag = docId.substring(0, splitIndex)
                        val path = docId.substring(splitIndex + 1)
                        val nonPrimaryVolume =
                            getPathToNonPrimaryVolume(context, tag)
                        if (nonPrimaryVolume != null) {
                            val result = "$nonPrimaryVolume/$path"
                            val file = File(result)
                            if (file.exists() && file.canRead()) {
                                return result
                            }
                        }
                    }
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    val selection = "_id=?"
                    val selectionArgs =
                        arrayOf(split[1])
                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }
            } else if ("content".equals(uri.scheme, ignoreCase = true)) {
                return getDataColumn(context, uri, null, null)
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }
            return null
        }

        lateinit var dialog: Dialog
        fun showProgressDialog(context: Context) {
            dialog = Dialog(context)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.setCancelable(false)
            dialog!!.setContentView(R.layout.dialog_progress_bar)
            dialog!!.show()
        }

        fun dismissProgressDialog() {
            if (dialog != null)
                dialog!!.dismiss()
        }
    }
}